const BASE_URL = 'https://rickandmortyapi.com/api/';

const getResource = async url => {
    const res = await fetch(`${BASE_URL}${url}`);
    if (!res.ok) throw new Error(`Could not fetch ${url}, received ${res.status}`);
    return await res.json();
};

export const getCharacters = async (nextPage = '') => {
    return await getResource(`/character${nextPage}`);
};