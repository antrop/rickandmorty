import React, { Component } from 'react';
import {BrowserRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {PagesRouter} from "./router/PagesRouter";
import Header from "./components/Header/Header";
import {loadCharacters} from "./actions/character";


import './App.css';


class App extends Component {
    constructor(props) {
        super(props);
        this.containerRef = React.createRef();
    }

    componentDidMount() {
        const {loadCharacters} = this.props;
        loadCharacters();

        document.addEventListener('scroll', this.loadOnScroll);
    }

    componentWillUnmount() {
       document.removeEventListener('scroll', this.loadOnScroll);
    }

    loadOnScroll = (e) =>{
        const {loading, nextPage, loadCharacters} = this.props;
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && !loading)  {
            loadCharacters(nextPage);
        }
    };

  render() {
    return (
        <BrowserRouter>
              <div ref={this.containerRef}>
                  <Header />
                  <PagesRouter />
              </div>
        </BrowserRouter>
    );
  }
}
const mapDispatchToProps = function (dispatch) {
    return {
        loadCharacters: bindActionCreators(loadCharacters, dispatch),
    };
};

const mapStateToProps = function (state) {
    return {
        nextPage: state.data.nextPage,
        loading: state.app.loading,
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
