import React from 'react';
import PropTypes from "prop-types";

import style from './Character.module.css';

const Character = ({name, image, onItemSelected}) => {
    return (
        <div className={style.container} onClick={onItemSelected}>
            <div>{name}</div>
            <img src={image} alt={name} />
        </div>
    );
};

Character.propTypes = {
    name: PropTypes.string,
    image: PropTypes.string,
    onItemSelected: PropTypes.func
};

export default Character;