import React from "react";
import { NavLink} from 'react-router-dom';

import style from './Header.module.css';

export const Items = [{
    "url": "/",
    "name": "Home"
},{
    "url": "/characters",
    "name": "Characters"
}];

const renderNavigationItem = () => {
    return Items.map(item => {
        return (
            <li key={item.url}>
                <NavLink to={item.url} activeClassName="activeLink">
                    <span>{item.name}</span>
                </NavLink>
            </li>
        );
    });
};


const Header = props => {
    return (
        <header className={style.header}>
            <ul>
                {renderNavigationItem()}
            </ul>
        </header>
    )
};

export default Header;
