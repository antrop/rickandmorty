import React from 'react';
import {connect} from "react-redux";
import Character from "../Character/Character";

const getCharacter = (id, characters) => {
    if (characters && characters.length) return characters[id - 1];
    return {};
};

const CharacterDetails = ({id, characters}) => {
    const character = getCharacter(id, characters);
    return (
        <Character
            name={character.name}
            image={character.image} />
    );
};

const mapStateToProps = function (state) {
    return {
        characters: state.data.characters,
    };
};

export default connect(mapStateToProps, null)(CharacterDetails);