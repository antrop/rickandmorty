import React from 'react';
import Character from "../Character/Character";
import {connect} from "react-redux";
import style from './CharactersList.module.css';

class CharactersList extends React.Component {
    constructor(props) {
        super(props);
        this.onItemSelected = this.onItemSelected.bind(this);
    }

    onItemSelected (id, history) {
        history.push(`/character/${id}`);
    }

    render () {
        const {characters, history} = this.props;
        return (
            <div className={style.container}>
                {characters.map(character =>
                        <Character
                            key={character.id}
                            name={character.name}
                            image={character.image}
                            onItemSelected={() => this.onItemSelected(character.id, history)}
                        />
                    )
                }
            </div>

        );
    }
}

const mapStateToProps = function (state) {
    return {
        characters: state.data.characters,
    };
};

export default connect(mapStateToProps, null)(CharactersList);
