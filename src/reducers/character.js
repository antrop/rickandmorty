import * as C from '../constants'

const initialState = {
    characters: [],
    nextPage: ''
};

export default function character(state = initialState, action) {
    switch (action.type) {
        case C.CHARACTER.LOADED:
            const characters = [...state.characters, ...action.payload];
            return { ...state, characters};
        case C.CHARACTER.NEXT_PAGE:
            const nextPage = action.payload;
            return { ...state, nextPage};
        default:
            return state;
    }
}

