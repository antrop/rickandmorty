import * as C from '../constants'

const initialState = {
    loading: false,
};

export default function common(state = initialState, action) {
    let loading;
    switch (action.type) {
        case C.APP.LOADING:
            loading = true;
            return { ...state, loading};
        case C.APP.LOADED:
            loading = false;
            return { ...state, loading};
        default:
            return state;
    }
}

