import { combineReducers } from 'redux';
import character from "./character";
import common from "./common";
export default combineReducers({
    app: common,
    data: character
});
