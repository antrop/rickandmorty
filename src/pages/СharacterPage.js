import React from 'react';
import { withRouter } from 'react-router-dom';
import CharacterDetails from "../components/CharacterDetails/CharacterDetails";

const СharacterPage = ({ history, match }) => {

    const { id } = match.params;

    return (
        <CharacterDetails id={id}  />
    );
};


export default withRouter(СharacterPage);