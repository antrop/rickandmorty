import React from 'react';
import CharactersList from "../components/CharactersList/CharactersList";
import {withRouter} from "react-router-dom";

class Home extends React.Component {

    render () {
        return (
            <div>
                <h2>Rick and Morty App</h2>

                <CharactersList history={this.props.history}/>
            </div>

        );
    }
}

export default withRouter(Home);
