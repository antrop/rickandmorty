import {getCharacters} from "../service/api";
import * as C from '../constants'

const _transformCharacter = (character) => {
    return {
        id: character.id,
        name: character.name,
        image: character.image
    }
};

const getNextPage = url => {
    return url.substring(url.indexOf('?'));
};

export const loadCharacters = nextPage => {
    return (dispatch) => {
        dispatch({type: C.APP.LOADING});

        getCharacters(nextPage).then(data => {
            dispatch({type: C.CHARACTER.LOADED, payload: data.results.map(_transformCharacter)});
            dispatch({type: C.CHARACTER.NEXT_PAGE, payload: getNextPage(data.info.next)});
            dispatch({type: C.APP.LOADED});
        });
    };
};
