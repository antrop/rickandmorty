import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Home from "../pages/Home";
import СharacterPage from "../pages/СharacterPage";
import CharactersPage from "../pages/CharactersPage";

export class PagesRouter extends React.Component {
    render() {
        return (
            <div className="container">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/characters" component={CharactersPage} />
                    <Route exact path="/character/:id" component={СharacterPage} />
                </Switch>
            </div>
        );
    }
}
