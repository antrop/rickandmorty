import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "../reducers/rootReducer";
import thunk from 'redux-thunk'

const middleware = applyMiddleware(thunk);

export const store = createStore(
    rootReducer,
    compose(middleware, window.devToolsExtension ? window.devToolsExtension() : f => f));
